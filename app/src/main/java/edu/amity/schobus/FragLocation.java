package edu.amity.schobus;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.DirectionsRoute;
import com.google.maps.model.TravelMode;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.amazonaws.mobile.client.internal.oauth2.OAuth2Client.TAG;

public class FragLocation  extends Fragment implements OnMapReadyCallback{

    private MapView mMapView;
    private AWSMobileClient Auth =AWSMobileClient.getInstance();
    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference myRef = database.getReference("up");
    private Location lastLocation;
    private static final int overview = 0;
    private TextView locationBox,drivername,busno, studentDetails;
    private LatLng latLng=null;
    private FloatingActionButton fab_zoomToLocation;
    private String url="https://adb1yf2mfl.execute-api.ap-south-1.amazonaws.com/dev";
    String email;
    private String busnumber,pname,dname,dphone;
    private ImageButton call;
    List<CharSequence> Students = new ArrayList<CharSequence>();
    Spinner spin;
    ImageView driverImage;
    private ArrayList<String> sname= new ArrayList<>();
    private ArrayList<String> sclass= new ArrayList<>();
    private int index=0, lengthResponse;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_location, container, false);

        fab_zoomToLocation=rootView.findViewById(R.id.jumpToLocation);
        drivername= rootView.findViewById(R.id.drivername);
        busno=rootView.findViewById(R.id.busno);
       // studentDetails = rootView.findViewById(R.id.student_name);
        call=rootView.findViewById(R.id.call_driver);
         spin =  ((Spinner) Objects.requireNonNull(getActivity()).findViewById(R.id.spinner));
         spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
             @Override
             public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 if(index!=position) {
                     index = position;
                     getdata(new VolleyCallBack() {
                         @Override
                         public void onSuccess() {
                             //Creating the ArrayAdapter instance having the country list

                             //getDriverImage();

                             sname=new ArrayList<>();
                             sclass=new ArrayList<>();

                             getBusData();

                         }
                     });
                 }
             }

             @Override
             public void onNothingSelected(AdapterView<?> parent) {

             }
         });
         driverImage= rootView.findViewById(R.id.DriverImage);

        //Creating the ArrayAdapter instance having the country list
      setupSpinnner();
        Objects.requireNonNull(getActivity()).findViewById(R.id.title_icon).setVisibility(View.VISIBLE);
       // Objects.requireNonNull(getActivity()).findViewById(R.id.student_name).setVisibility(View.VISIBLE);

        setHasOptionsMenu(true);

        try {
            MapsInitializer.initialize(Objects.requireNonNull(getActivity()));
            mMapView = rootView.findViewById(R.id.mapview);
            mMapView.onCreate(savedInstanceState);
            mMapView.getMapAsync(this);
        } catch (InflateException e) {
            Toast.makeText(getActivity(), "NULL@FRAGLOCATION.JAVA", Toast.LENGTH_LONG).show();
        }
        getdata(new VolleyCallBack() {
            @Override
            public void onSuccess() {
                //Creating the ArrayAdapter instance having the country list

                //getDriverImage();

                getBusData();
                setupSpinnner();

            }
        });
        locationBox = rootView.findViewById(R.id.location_Box);
        return rootView;
    }

    private void setupSpinnner(){

        for(int i=0;i<lengthResponse;i++) {

            String name = getColoured(sname.get(i), "#000000");
            String standard = getColoured(" Class " + sclass.get(i), "#808080");
            //  Spanned varHtml = Html.fromHtml(name+ " Class "+standard);
            Students.add(Html.fromHtml(name + standard));
        }
        ArrayAdapter<CharSequence> aa = new ArrayAdapter<>(Objects.requireNonNull(getActivity()),android.R.layout.simple_spinner_item,Students);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);


    }

    private String getColoured(String text,String color){
        return "<font color=" + color+">"+text+"</font>";
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbacontactsr here for different fragments different titles
    }

    @Override
    public void onCreateOptionsMenu(
            @NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.route_menu, menu);
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {

        mapCustomisation(googleMap);
        googleMap.setTrafficEnabled(true);

        DirectionsResult results = getDirectionsDetails("Raipur ,near vikrant palace Opp. Gate no.2 Amity university, Raipur Khadar, Sector 126, Noida, Uttar Pradesh 201304","Amity Road, Sector 125, Noida, Uttar Pradesh 201313",TravelMode.DRIVING);
        if (results != null) {
            addPolyline(results, googleMap);
            positionCamera(results.routes[overview], googleMap);
            addMarkersToMap(results, googleMap);
        }

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String latitude = "" + dataSnapshot.child("latitude").getValue(Double.class);
                String longitude = "" + dataSnapshot.child("longitude").getValue(Double.class);
                changeLocation(googleMap, latitude, longitude);
                latLng=new LatLng(Double.parseDouble(latitude),Double.parseDouble(longitude));
                Location location = new Location("SchoolTracker app");
                location.setLatitude(Double.parseDouble(latitude));
                location.setLongitude(Double.parseDouble(longitude));
                lastLocation=location;
                getAddressFromLocation(location);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


    }

    private Marker marker = null;

    private void changeLocation(GoogleMap googleMap, String latitude, String longitude) {
        float zoom = googleMap.getMaxZoomLevel() - 5;
        if (marker != null) {
            marker.remove();
            zoom = googleMap.getCameraPosition().zoom;
        }
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)), zoom));
        marker = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude))));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onDetach() {
        super.onDetach();
    }


    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void mapCustomisation(final GoogleMap googleMap) {

        fab_zoomToLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(latLng!=null) {
                    float zoom = googleMap.getMaxZoomLevel() - 5;
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
                }
            }
        });
        final int valueInPx_collapsed = (int) (120 * getResources().getDisplayMetrics().density);
        googleMap.setPadding(0, 0, 0, valueInPx_collapsed);

    }

    private void getAddressFromLocation(Location location) {

        // In some rare cases the location returned can be null
        if (location == null) {
            return;
        }

        if (!Geocoder.isPresent()) {
            Toast.makeText(getActivity(),
                    "no_geocoder_available",
                    Toast.LENGTH_LONG).show();
            return;
        }

        // Start service and update UI to reflect new location
        startIntentService();


    }

    private void startIntentService() {
        try {
            Intent intent = new Intent(getActivity(), FetchAddressIntentService.class);
            AddressResultReceiver resultReceiver = new AddressResultReceiver(new Handler());
            intent.putExtra(Constants.RECEIVER, resultReceiver);
            intent.putExtra(Constants.LOCATION_DATA_EXTRA, lastLocation);
            Objects.requireNonNull(getActivity()).startService(intent);
        }catch (NullPointerException e){
            //Toast.makeText(getContext(), "NPE", Toast.LENGTH_LONG).show();
        }
    }


    class AddressResultReceiver extends ResultReceiver {
        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            try {

                if (resultData == null) {
                    return;
                }

                // Display the address string
                // or an error message sent from the intent service.
                String addressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
                if (addressOutput == null) {
                    addressOutput = "";
                }

                // Show a toast message if an address was found.
                if (resultCode == Constants.SUCCESS_RESULT) {
                    locationBox.setText(addressOutput);
                }

            }catch(NullPointerException e){
                Toast.makeText(getActivity(),
                        "reached",
                        Toast.LENGTH_LONG).show();
            }

        }

    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext.setQueryRateLimit(3)
                .setApiKey(getString(R.string.directionsApiKey))
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
    }


    private DirectionsResult getDirectionsDetails(String origin, String destination, TravelMode mode) {
        DateTime now = new DateTime();
        try {
            return DirectionsApi.newRequest(getGeoContext())
                    .mode(mode)
                    .origin(origin)
                    .destination(destination)
                    .departureTime(now)
                    .await();
        } catch (ApiException | IOException e) {
            e.printStackTrace();
            Toast.makeText(getActivity(), ""+e , Toast.LENGTH_LONG).show();
            return null;
        } catch (InterruptedException e) {
            Toast.makeText(getActivity(), ""+e , Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return null;
        }
    }


    private void addMarkersToMap(DirectionsResult results, GoogleMap mMap) {
        mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[overview].legs[overview].startLocation.lat,results.routes[overview].legs[overview].startLocation.lng)).title(results.routes[overview].legs[overview].startAddress));
        mMap.addMarker(new MarkerOptions().position(new LatLng(results.routes[overview].legs[overview].endLocation.lat,results.routes[overview].legs[overview].endLocation.lng)).title(results.routes[overview].legs[overview].startAddress).snippet(getEndLocationTitle(results)));
    }

    private void positionCamera(DirectionsRoute route, GoogleMap mMap) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(route.legs[overview].startLocation.lat, route.legs[overview].startLocation.lng), 12));
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap) {
        List<LatLng> decodedPath = PolyUtil.decode(results.routes[overview].overviewPolyline.getEncodedPath());
        mMap.addPolyline(new PolylineOptions().addAll(decodedPath));
    }

    private String getEndLocationTitle(DirectionsResult results){
        return  "Time :"+ results.routes[overview].legs[overview].duration.humanReadable + " Distance :" + results.routes[overview].legs[overview].distance.humanReadable;
    }
    private void getdata(final VolleyCallBack callback){
        RequestQueue queue= Volley.newRequestQueue(Objects.requireNonNull(getActivity()).getApplicationContext());

        JsonArrayRequest jsonArrayRequest= new JsonArrayRequest
                (Request.Method.GET, url+"/getStudent?scode=s123&email="+Auth.getUsername(), null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try {

                            lengthResponse=response.length();
                            busnumber= response.getJSONObject(index).get("busno").toString();
                            pname=response.getJSONObject(index).get("pname").toString();
                            for(int i=0;i<response.length();i++){

                                sname.add(response.getJSONObject(i).get("sname").toString());
                                sclass.add(response.getJSONObject(i).get("sclass").toString());


                            }
                            busno.setText(busnumber);
                            Objects.requireNonNull(getActivity()).setTitle(pname);



                         //((TextView)Objects.requireNonNull(getActivity()).findViewById(R.id.student_name))
                               //  .setText(Html.fromHtml(name+ " Class "+standard));

                            callback.onSuccess();
                        }catch (Exception e){
                            Toast.makeText(getActivity(), ""+e, Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), ""+error.toString(), Toast.LENGTH_LONG).show();
                    }

                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                String token="";
                try{
                    token=Auth.getTokens().getIdToken().getTokenString();
                }catch (Exception e){
                    Toast.makeText(getActivity(), "err", Toast.LENGTH_LONG).show();
                }
                headers.put("Authorization", token);
                return headers;
            }
        };

        queue.add(jsonArrayRequest);


    }

    private void getBusData(){
        RequestQueue queue= Volley.newRequestQueue(Objects.requireNonNull(getActivity()).getApplicationContext());

        JsonArrayRequest jsonArrayBusRequest= new JsonArrayRequest
                (Request.Method.GET, url+"/getDriver?bcode=b123&bno="+busnumber, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            dname= response.getJSONObject(0).get("dname").toString();
                            dphone=response.getJSONObject(0).get("dphoneno").toString();
                            drivername.setText(dname);
                            call.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions(getActivity(),
                                                new String[]{Manifest.permission.CALL_PHONE},
                                                1);
                                    }
                                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        Toast.makeText(getActivity(), "Calling Permission Denied!", Toast.LENGTH_SHORT).show();

                                    } else{
                                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                                        callIntent.setData(Uri.parse("tel:+91" + dphone));
                                        startActivity(callIntent);
                                    }
                                }
                            });
                        }catch (Exception e){
                            Toast.makeText(getActivity(), "BusResponse: "+e, Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), "BusRequest:"+error.toString(), Toast.LENGTH_LONG).show();
                    }

                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                String token="";
                try{
                    token=Auth.getTokens().getIdToken().getTokenString();
                }catch (Exception e){
                    Toast.makeText(getActivity(), "err", Toast.LENGTH_LONG).show();
                }
                headers.put("Authorization", token);
                return headers;
            }
        };
        queue.add(jsonArrayBusRequest);




    }

    private void getDriverImage(){
        RequestQueue queue= Volley.newRequestQueue(Objects.requireNonNull(getActivity()).getApplicationContext());

        JsonObjectRequest jsonObjectBusRequest= new JsonObjectRequest
                (Request.Method.GET, url+"/getthepresignedurl?myKey="+busnumber+".jpeg", null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Picasso.get()
                                    .setIndicatorsEnabled(true);
                            Picasso.get().load(response.getString("url"))
                                    .transform(new CircleTransform())
                                    .into(driverImage, new Callback() {
                                @Override
                                public void onSuccess() {

                                }
                                @Override
                                public void onError(Exception e) {
                                    Toast.makeText(getActivity(), ""+e, Toast.LENGTH_LONG).show();
                                }
                            });

                        }catch (Exception e){
                            Toast.makeText(getActivity(), ""+e, Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getActivity(), ""+error.toString(), Toast.LENGTH_LONG).show();
                    }

                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                String token="";
                try{
                    token=Auth.getTokens().getIdToken().getTokenString();
                }catch (Exception e){
                    Toast.makeText(getActivity(), "err", Toast.LENGTH_LONG).show();
                }
                headers.put("Authorization", token);
                return headers;
            }
        };
        queue.add(jsonObjectBusRequest);



    }

}

