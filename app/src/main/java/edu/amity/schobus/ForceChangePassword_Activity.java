package edu.amity.schobus;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.SignInResult;
import com.amazonaws.mobile.client.results.SignInState;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForceChangePassword_Activity extends AppCompatActivity {

    private AWSMobileClient Auth= AWSMobileClient.getInstance();
    private EditText newPass,newPass_retype;
    private CheckBox showPass;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_force_change_password_);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        //Views
        newPass = findViewById(R.id.newPass);
        newPass_retype = findViewById(R.id.newPassRetype);
        Button changePass= findViewById(R.id.changePasswordButton);
        showPass = findViewById(R.id.showPassword);
        progressBar=findViewById(R.id.progressBar_forceChangePassword);

        changePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(newPass_retype.getWindowToken(), 0);

                progressBar.setVisibility(View.VISIBLE);

                if(isValidPassword(newPass.getText().toString().trim())&&newPass.getText().toString().trim().equals(newPass_retype.getText().toString().trim()))
                {
                    clickChange();
                }
                else{
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(ForceChangePassword_Activity.this, "Password invalid or don't match", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void passwordShowToggle(View view){

        if(showPass.isChecked()){
            newPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            newPass_retype.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }else{
            newPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
            newPass_retype.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }

    }

    private boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();

    }


    private void clickChange(){

        if((newPass.getText().toString()).equals("")&&(newPass_retype.getText().toString()).equals("")){
            Toast.makeText(this, "Empty Fields", Toast.LENGTH_SHORT).show();
        }
        else {

            AWSMobileClient.getInstance().confirmSignIn(newPass.getText().toString().trim(), new Callback<SignInResult>() {
                @Override
                public void onResult(SignInResult signInResult) {
                    if (signInResult.getSignInState() == SignInState.DONE) {
                        Intent intent = new Intent(ForceChangePassword_Activity.this, MainActivity.class);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                        });
                        startActivity(intent);
                        finish();
                        //case SMS_MFA:
                        //    Auth.signOut();
                        //    break;
                    } else {
                        Auth.signOut();
                    }
                }

                @Override
                public void onError(final Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(ForceChangePassword_Activity.this, "Error: " + e, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }
    }
}
