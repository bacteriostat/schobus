package edu.amity.schobus;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class AddContactActivity extends AppCompatActivity {

    FirebaseFirestore db = FirebaseFirestore.getInstance();
    final String TAG= "My_TAG";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        setTitle("Add Contact");
        Button add= findViewById(R.id.add);
        Button cancel=findViewById(R.id.cancel);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addContact();
                finish();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


    }

    private void addContact(){
        EditText name= findViewById(R.id.name_contact);
        EditText number= findViewById(R.id.number);
        Map<String, String> details = new HashMap<>();
        if(number.getText().toString().length()==10) {
            details.put("Number", number.getText().toString());
            db.collection("List of Contacts").document(name.getText().toString()).set(details);
        }else{
            Toast.makeText(this, "Invalid Phone", Toast.LENGTH_SHORT).show();
        }
    }
}
