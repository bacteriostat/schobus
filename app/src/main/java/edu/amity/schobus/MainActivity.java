package edu.amity.schobus;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.config.AWSConfiguration;
import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private AWSMobileClient Auth =AWSMobileClient.getInstance();
    private String url="https://adb1yf2mfl.execute-api.ap-south-1.amazonaws.com/dev";
    Intent intentFetchAddressIntentSevice;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    String[] country = { "India", "USA", "China", "Japan", "Other"};

    boolean isHome=true;
    public String getUserID(){
        return (Objects.requireNonNull(Auth.getUsername()))+"";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Layout Setup
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername =  headerView.findViewById(R.id.emailNavigationHeader);
        navUsername.setText(getUserID());
        navigationView.setNavigationItemSelectedListener(this);
        displaySelectedScreen(R.id.nav_location);

        //AppSyncClient Initialisation
        AWSAppSyncClient mAWSAppSyncClient = AWSAppSyncClient.builder()
                .context(getApplicationContext())
                .awsConfiguration(new AWSConfiguration(getApplicationContext()))
                .cognitoUserPoolsAuthProvider(new CognitoUserPoolsAuthProvider() {
                    @Override
                    public String getLatestAuthToken() {
                        try {
                            return AWSMobileClient.getInstance().getTokens().getIdToken().getTokenString();
                        } catch (Exception e) {
                            Log.e("APPSYNC_ERROR", e.getLocalizedMessage());
                            return e.getLocalizedMessage();
                        }
                    }
                }).build();

        RequestQueue queue= Volley.newRequestQueue(this);

        JsonArrayRequest jsonArrayRequest= new JsonArrayRequest
                (Request.Method.GET, url+"/getRoute?bno=UP243456", null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                           // Toast.makeText(MainActivity.this, "" + response.getJSONObject(0).get("route"), Toast.LENGTH_LONG).show();
                        }catch (Exception e){}
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(MainActivity.this, ""+error.toString(), Toast.LENGTH_LONG).show();
                    }

                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                String token="";
                try{
                    token=Auth.getTokens().getIdToken().getTokenString();
                }catch (Exception e){
                    Toast.makeText(MainActivity.this, "err", Toast.LENGTH_LONG).show();
                }
                headers.put("Authorization", token);
                return headers;
            }
        };
        queue.add(jsonArrayRequest);

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(!isHome){
            displaySelectedScreen(R.id.nav_location);
        }else{
            finish();
        }
    }

    //displaySelectedScreen
    public void displaySelectedScreen(int itemId) {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.getMenu().findItem(itemId).setChecked(true);
        //creating fragment object
        Fragment fragment = null;

        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_location:
                fragment = new FragLocation();
                break;
            case R.id.nav_route:
                fragment = new FragRoute();
                break;
            case R.id.nav_notifications:
                fragment = new FragNotifications();
                break;
           case R.id.nav_report_issue:
                fragment = new FragReportIssue();
                break;
            case R.id.nav_signout:
                finish();
                signOut();
                break;

        }

        //replacing the fragment
       if (fragment != null ) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
           if(itemId!=R.id.nav_location){
               isHome=false;
               toggle.setDrawerIndicatorEnabled(false);
               getSupportActionBar().setDisplayHomeAsUpEnabled(true);
               (findViewById(R.id.title_icon)).setVisibility(View.GONE);
               (findViewById(R.id.spinner)).setVisibility(View.GONE);
           }else{
               isHome=true;
               getSupportActionBar().setDisplayHomeAsUpEnabled(false);
               (findViewById(R.id.spinner)).setVisibility(View.VISIBLE);

               toggle.setDrawerIndicatorEnabled(true);
               Toolbar toolbar = findViewById(R.id.toolbar);
               toggle = new ActionBarDrawerToggle(
                       this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
           }
            ft.replace(R.id.content_frame, fragment);
            ft.commit();

       }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }



    @Override
    public boolean onNavigationItemSelected (@NonNull MenuItem item){        // Handle navigation view item clicks here.
        displaySelectedScreen(item.getItemId());
        return true;
    }

    //Sign Out
    private void signOut() {
        Auth.signOut();
        if(intentFetchAddressIntentSevice!=null){
            stopService(intentFetchAddressIntentSevice);
            intentFetchAddressIntentSevice=null;}
            Intent intent= new Intent(this,SignInActivity.class);
            startActivity(intent);
    }

    public void getdata(){

    }

}



