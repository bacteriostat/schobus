package edu.amity.schobus;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class notifications_adapter extends RecyclerView.Adapter<notifications_adapter.MyViewHolder> {
    private ArrayList<String> notification;
    private ArrayList<String> description;
    private ArrayList<String> datetime;




    notifications_adapter(ArrayList<String> notification,ArrayList<String> description,ArrayList<String> datetime){

        this.notification=notification;
        this.description=description;
        this.datetime=datetime;


    }
    @NonNull
    @Override
    public notifications_adapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_notifications, viewGroup, false);
        return new notifications_adapter.MyViewHolder(v);
    }




    @Override
    public void onBindViewHolder(@NonNull notifications_adapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.notification.setText(notification.get(i));
        myViewHolder.description.setText(description.get(i));
        myViewHolder.datetime.setText(datetime.get(i));

    }

    @Override
    public int getItemCount() {
        return notification.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView notification;
        public TextView description;
        public TextView datetime;

        private MyViewHolder(@NonNull View itemView) {
            super(itemView);

            notification= itemView.findViewById(R.id.notifications_text);
            description=itemView.findViewById(R.id.description_text);
            datetime= itemView.findViewById(R.id.datetime_text);



        }
    }
}
