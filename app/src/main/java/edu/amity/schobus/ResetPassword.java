package edu.amity.schobus;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.ForgotPasswordResult;
import com.amazonaws.mobile.client.results.ForgotPasswordState;

public class ResetPassword extends AppCompatActivity {

    private EditText newPass,newPass_retype,user,code;
    private CheckBox showPass;
    private Button next;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        //Views
        newPass = findViewById(R.id.newPass);
        newPass_retype = findViewById(R.id.newPassRetype);
        showPass = findViewById(R.id.showPassword);
        next=findViewById(R.id.nextButton);
        user=findViewById(R.id.Username);
        code=findViewById(R.id.VerificationCode);
        progressBar=findViewById(R.id.progress_bar_resetPassword);
    }

    public void passwordShowToggle(View view) {
        if(showPass.isChecked()){
            newPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            newPass_retype.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }else{
            newPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
            newPass_retype.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    public void changeScreen(View view) {

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(code.getWindowToken(), 0);

        if((user.getText().toString()).equals("")){
            Toast.makeText(this, "Empty Fields", Toast.LENGTH_SHORT).show();
        }
        else {


            progressBar.setVisibility(View.VISIBLE);

            user.setVisibility(View.GONE);
            next.setVisibility(View.GONE);

            AWSMobileClient.getInstance().forgotPassword(user.getText().toString(), new Callback<ForgotPasswordResult>() {
                @Override
                public void onResult(final ForgotPasswordResult result) {
                    if (result.getState() == ForgotPasswordState.CONFIRMATION_CODE) {
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(ResetPassword.this, "Verification code sent!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onError(Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ResetPassword.this, "Error!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            });
        }

    }

    public void changePass(View view) {

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(newPass_retype.getWindowToken(), 0);

        if((code.getText().toString()).equals("")&&(newPass.getText().toString()).equals("")&&(newPass_retype.getText().toString()).equals("")){
            Toast.makeText(this, "Empty Fields", Toast.LENGTH_SHORT).show();
        }
        else {

            progressBar.setVisibility(View.VISIBLE);

            AWSMobileClient.getInstance().confirmForgotPassword(newPass.getText().toString(), code.getText().toString(), new Callback<ForgotPasswordResult>() {
                @Override
                public void onResult(final ForgotPasswordResult result) {
                    if (result.getState() == ForgotPasswordState.DONE) {
                        Toast.makeText(ResetPassword.this, "Password changed successfully!", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.INVISIBLE);
                        finish();
                    }
                }


                @Override
                public void onError(Exception e) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(ResetPassword.this, "Error! Please try again.", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
                }
            });

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
