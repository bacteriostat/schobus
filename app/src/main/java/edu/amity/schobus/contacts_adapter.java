package edu.amity.schobus;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class contacts_adapter extends RecyclerView.Adapter<contacts_adapter.MyViewHolder> {

    private ArrayList<String> name;
    private ArrayList<String> number;
    private Context mContext;
    private FragmentManager fm;


    contacts_adapter(ArrayList<String> name, ArrayList<String> number, Context contexts, FragmentManager fm) {

        this.name = name;
        this.number = number;
        this.mContext = contexts;
        this.fm=fm;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_contacts, viewGroup, false);
        return new contacts_adapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final contacts_adapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.name.setText(name.get(i));
        myViewHolder.number.setText(number.get(i));
        myViewHolder.setClickListener(new ItemClickListener()
        {
            @Override
            public void onClick(View view, int position,boolean isLongClick) {

               if (isLongClick){
                   DialogFragment fragment= new DeleteContacts();
                   Bundle bundle=new Bundle();
                   bundle.putString("delete",name.get(position));
                   fragment.setArguments(bundle);
                   fragment.show(fm, "delete_dialog");

               }
                        else {
                   if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                       Toast.makeText(mContext, "Calling Permission Denied!", Toast.LENGTH_SHORT).show();

                   } else {
                       Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+91" + number.get(position)));
                       mContext.startActivity(intent);
                   }
               }

            }
        });



    }

    @Override
    public int getItemCount() {
        return name.size();
    }



    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,View.OnLongClickListener{
        public TextView name;
        public TextView number;
        private ItemClickListener clickListener;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            name= itemView.findViewById(R.id.contacts_item);
            number= itemView.findViewById(R.id.number_item);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);



            }
        private void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
             clickListener.onClick(view, getLayoutPosition(),false);

        }


        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getLayoutPosition(),true);
            return true;
        }
    }
}
