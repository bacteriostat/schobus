package edu.amity.schobus;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;


public class route_adapter extends Adapter<route_adapter.MyViewHolder> {

    private ArrayList<String> name;
    private ArrayList<String> route;
    private ArrayList<String> time;




    route_adapter(ArrayList<String> name,ArrayList<String> route,ArrayList<String> time){

        this.name=name;
        this.route=route;
        this.time=time;


        }
@NonNull
@Override
public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_view_route, viewGroup, false);
        return new route_adapter.MyViewHolder(v);
        }

@Override
public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        myViewHolder.name.setText(name.get(i));
    myViewHolder.route.setText(route.get(i));
    myViewHolder.time.setText(time.get(i));


}

@Override
public int getItemCount() {
        return name.size();
        }

public static class MyViewHolder extends RecyclerView.ViewHolder{
    public TextView name;
    public TextView route;
    public TextView time;

    private MyViewHolder(@NonNull View itemView) {
        super(itemView);

        name= itemView.findViewById(R.id.route_item);
        route=itemView.findViewById(R.id.route);
        time= itemView.findViewById(R.id.time_item);




    }
}
}
