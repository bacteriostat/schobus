package edu.amity.schobus;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.results.SignInResult;
import com.amazonaws.services.cognitoidentityprovider.model.PasswordResetRequiredException;

public class SignInActivity extends AppCompatActivity implements
        View.OnClickListener  {
    private static final String TAG = "EmailPassword";

    private EditText mUsername;
    private EditText mPasswordField;
    private AWSMobileClient Auth;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        //Views
        mUsername = findViewById(R.id.field_adm_code);
        mPasswordField = findViewById(R.id.fieldPassword);
        findViewById(R.id.emailSignInButton).setOnClickListener(this);

        //AWSMobileClient
        Auth= AWSMobileClient.getInstance();

        //Progress Bar
        progressBar= findViewById(R.id.progressBar_signInActivity);
    }


    //Sign In Funtion
    private void signIn(@NonNull String email, String password) {
        Auth.signIn(email.toLowerCase(), password, null, new Callback<SignInResult>() {
            @Override
            public void onResult(final SignInResult signInResult) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d(TAG, "Sign-in callback state: " + signInResult.getSignInState());
                        switch (signInResult.getSignInState()) {
                            case DONE:
                                try {
                                    Auth.getTokens().getIdToken().getTokenString();
                                }catch (Exception e){}
                                Intent intent = new Intent(SignInActivity.this, MainActivity.class);
                                progressBar.setVisibility(View.INVISIBLE);
                                finish();
                                startActivity(intent);
                                break;
                            case NEW_PASSWORD_REQUIRED:
                                ForceChangePassword();
                                break;

                            default:
                                progressBar.setVisibility(View.INVISIBLE);
                                Toast.makeText(SignInActivity.this, ""+signInResult.getSignInState(), Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });
            }
            @Override
            public void onError(Exception e) {
                final Exception e_copy=e;
                Log.e(TAG, "Sign-in error", e);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(e_copy.getClass()==PasswordResetRequiredException.class){
                            Intent intent = new Intent(SignInActivity.this, ResetPassword.class);
                            startActivity(intent);
                        }else{
                            TextView error = findViewById(R.id.signInError);
                            error.setVisibility(View.VISIBLE);
                        }
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                });

            }
        });

    }

    //Force Change Password
    private void ForceChangePassword(){
        Intent intent = new Intent(SignInActivity.this, ForceChangePassword_Activity.class);
        startActivity(intent);
        progressBar.setVisibility(View.GONE);
        finish();
    }

    //OnClick() method for sign in button
    @Override
    public void onClick(View view) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mPasswordField.getWindowToken(), 0);
        TextView error = findViewById(R.id.signInError);
        error.setVisibility(View.GONE);
        if((mUsername.getText().toString()).equals("")&&(mPasswordField.getText().toString()).equals("")){
            Toast.makeText(this, "Empty Fields", Toast.LENGTH_SHORT).show();
        }
        else {
            progressBar.setVisibility(View.VISIBLE);
            signIn(mUsername.getText().toString(), mPasswordField.getText().toString());
            String email=mUsername.getText().toString();

        }
    }

    //Show Password Toggle
    public void passwordShowToggle(View view) {
        CheckBox showPass=findViewById(R.id.showPassword);
        if(showPass.isChecked()){
            mPasswordField.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else{
            mPasswordField.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    //Reset Password
    public void ResetPassword(View view) {
        Intent intent = new Intent(SignInActivity.this, ResetPassword.class);
        startActivity(intent);
    }
}
