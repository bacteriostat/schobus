package edu.amity.schobus;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Objects;

public class FragRoute extends Fragment {

    private ProgressBar progressBar;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View v= inflater.inflate(R.layout.fragment_route, container, false);

        Toolbar toolbar =  getActivity().findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity)getActivity()).displaySelectedScreen(R.id.nav_location);
            }
        });

        //Views
        ExtendedFloatingActionButton track = v.findViewById(R.id.track);
        final ArrayList<String > route =new ArrayList<>();
        final ArrayList<String> time =new ArrayList<>();
        final ArrayList<String> area =new ArrayList<>();
        progressBar=v.findViewById(R.id.progressBar_fragRoute);

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        final String TAG = "My_TAG";
        final route_adapter adapter=new route_adapter(route,area, time);
        setHasOptionsMenu(true);
        db.collection("List of Route")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                                //Log.d(TAG, document.getId() + " => " + document.getData());
                                area.add(Objects.requireNonNull(document.getId()));
                                route.add(Objects.requireNonNull(document.getData().get("Route")).toString()+",");
                                time.add(Objects.requireNonNull(document.getData().get("Time")).toString());
                            }
                            adapter.notifyDataSetChanged();
                            progressBar.setVisibility(View.GONE);
                        } else {
                            Log.w(TAG, "Error getting documents.", task.getException());
                        }
                    }
                });
        RecyclerView recyclerView= v.findViewById(R.id.route_rcview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        track.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) Objects.requireNonNull(getActivity())).displaySelectedScreen(R.id.nav_location);
            }
        });

        return v;

    }
    @Override
    public void onCreateOptionsMenu(
            @NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.route_menu, menu);
        MenuItem menuItem=menu.findItem(R.id.action_home);
        menuItem.setVisible(true);
    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id== R.id.action_home) {
            ((MainActivity) Objects.requireNonNull(getActivity())).displaySelectedScreen(R.id.nav_location);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        Objects.requireNonNull(getActivity()).setTitle("");
    }

}
